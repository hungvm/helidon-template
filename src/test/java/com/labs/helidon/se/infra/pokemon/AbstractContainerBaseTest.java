package com.labs.helidon.se.infra.pokemon;

import com.labs.helidon.se.Server;
import io.helidon.config.Config;
import org.testcontainers.containers.JdbcDatabaseContainer;
import org.testcontainers.containers.PostgreSQLContainerProvider;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractContainerBaseTest {
    static Config appConfig;
    static final JdbcDatabaseContainer<?> postgres;

    static {
        postgres = new PostgreSQLContainerProvider().newInstance("12.6-alpine")
                .withDatabaseName("pokemon")
                .withUsername("postgres")
                .withPassword("123456");
        postgres.start();
        Map<String, String> env = new HashMap<>();
        env.put("db.connection.url", postgres.getJdbcUrl());
        env.put("db.connection.username", "postgres");
        env.put("db.connection.password", "123456");
        appConfig = Server.customConfig(env);
    }
}
