FROM registry.entrade.com.vn/dockerhub/library/eclipse-temurin:21-jre-alpine

COPY target/se.jar /usr/app/se.jar

WORKDIR /usr/app
EXPOSE 8080
ENTRYPOINT ["java", "-XX:MaxRAMPercentage=80", "-jar", "se.jar"]
