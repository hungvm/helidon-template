package com.labs.helidon.se;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import com.labs.helidon.se.domain.ApplicationException;
import com.labs.helidon.se.infra.RequestLoggingFilter;
import com.labs.helidon.se.infra.health.HealthController;
import com.labs.helidon.se.infra.pokemon.PokemonController;
import com.labs.helidon.se.infra.pokemon.PokemonRepository;
import com.labs.helidon.se.infra.security.JwtFilter;
import com.labs.helidon.se.infra.security.RouteMatcher;
import com.labs.helidon.se.infra.security.String2KeyConverter;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import io.helidon.common.context.Contexts;
import io.helidon.config.Config;
import io.helidon.config.ConfigSources;
import io.helidon.http.Method;
import io.helidon.http.Status;
import io.helidon.http.media.MediaContext;
import io.helidon.http.media.jackson.JacksonSupport;
import io.helidon.webserver.WebServerConfig;
import io.helidon.webserver.http.HttpRouting;
import lombok.extern.slf4j.Slf4j;
import org.flywaydb.core.Flyway;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.List;
import java.util.Map;

import static com.labs.helidon.se.domain.ErrorCode.INTERNAL_SERVER_ERROR;

@Slf4j
public class Server {
    private final Config rootConfig;
    private final Map<String, List<Method>> jwtWhitelist;

    public Server(Config config, Map<String, List<Method>> jwtWhitelist) {
        this.rootConfig = config;
        this.jwtWhitelist = jwtWhitelist;
    }

    public static NamedParameterJdbcTemplate createDbClient(Config dbConfig) {
        String url = dbConfig.get("connection.url").asString().orElseThrow();
        String username = dbConfig.get("connection.username").asString().orElseThrow();
        String password = dbConfig.get("connection.password").asString().orElseThrow();
        int maximumPoolSize = dbConfig.get("connection.maximumPoolSize").asInt().orElseThrow();
        int minimumIdle = dbConfig.get("connection.minimumIdle").asInt().orElseThrow();
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(url);
        config.setUsername(username);
        config.setPassword(password);
        config.setDriverClassName("org.postgresql.Driver");
        config.setMaximumPoolSize(maximumPoolSize);
        config.setMinimumIdle(minimumIdle);
        HikariDataSource ds = new HikariDataSource(config);
        return new NamedParameterJdbcTemplate(ds);
    }

    public NamedParameterJdbcTemplate getDbClient() {
        NamedParameterJdbcTemplate dbClient = Contexts.globalContext().get(NamedParameterJdbcTemplate.class).orElse(null);
        if (dbClient == null) {
            log.info("setupDBConnection");
            dbClient = createDbClient(rootConfig.get("db"));
            Contexts.globalContext().register(dbClient);
        }
        return dbClient;
    }

    public static ObjectMapper setupObjectMapper() {
        ObjectMapper objectMapper = Contexts.globalContext().get(ObjectMapper.class).orElse(null);
        if (objectMapper == null) {
            objectMapper = new ObjectMapper()
                    .registerModule(new ParameterNamesModule())
                    .registerModule(new Jdk8Module())
                    .registerModule(new JavaTimeModule());
            objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
            objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            Contexts.globalContext().register(objectMapper);
        }
        return objectMapper;
    }

    public static void migrate(Config appConfig) {
        var config = appConfig.get("db");
        if (config.get("migrate").asBoolean().orElse(false)) {
            String url = config.get("connection.url").asString().orElseThrow();
            String username = config.get("connection.username").asString().orElseThrow();
            String password = config.get("connection.password").asString().orElseThrow();
            log.info("migrate {}", url);
            Flyway flyway = Flyway.configure()
                    .dataSource(url, username, password)
                    .locations("classpath:db/migration")
                    .outOfOrder(true)
                    .load();
            flyway.migrate();
        }
    }

    public static void setupMediaContext(WebServerConfig.Builder builder) {
        ObjectMapper objectMapper = setupObjectMapper();
        var mediaContext = MediaContext.builder().addMediaSupport(JacksonSupport.create(objectMapper)).build();
        builder.mediaContext(mediaContext);
    }

    public void shutdown() {
        log.info("server is shutting down");
    }

    private void setupAppContext() {
        migrate(this.rootConfig);
        var dbClient = getDbClient();
        PokemonRepository pokemonRepository = new PokemonRepository(dbClient);
        Contexts.globalContext().register(pokemonRepository);

        PokemonController pokemonController = new PokemonController(pokemonRepository);
        Contexts.globalContext().register(pokemonController);
    }

    public void routing(HttpRouting.Builder routing) {
        setupAppContext();
        String publicKey = rootConfig.get("jwt.publicKey").asString().orElseThrow();
        String2KeyConverter converter = new String2KeyConverter();
        RouteMatcher skipMatcher = new RouteMatcher(this.jwtWhitelist);
        PokemonController pokemonController = Contexts.globalContext().get(PokemonController.class).orElseThrow(() -> new RuntimeException("PokemonController not init"));
        routing
                .error(Exception.class, (req, res, ex) -> {
                    log.error("fail to process {} {}", req.path().path(), ex.getMessage(), ex);
                    res.status(Status.INTERNAL_SERVER_ERROR_500);
                    res.send(INTERNAL_SERVER_ERROR.setCustomMessage(ex.getMessage()));
                })
                .error(ApplicationException.class, (req, res, ex) -> {
                    log.warn("fail to process {} {}", req.path().path(), ex.getErrorCode());
                    res.status(ex.getErrorCode().getStatus());
                    res.send(ex.getErrorCode());
                })
                .addFilter(new RequestLoggingFilter())
                .addFilter(new JwtFilter(skipMatcher, converter.convertToPublicKey(publicKey)))
                .register("/", new HealthController(), pokemonController);
    }

    public static Config customConfig(Map<String, String> properties) {
        Config existedConfig = Config.global();
        return Config.builder()
                .sources(ConfigSources.create(properties), ConfigSources.create(existedConfig))
                .build();
    }
}
