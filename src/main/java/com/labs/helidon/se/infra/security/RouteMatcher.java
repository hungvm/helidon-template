package com.labs.helidon.se.infra.security;

import io.helidon.http.HttpPrologue;
import io.helidon.http.Method;
import io.helidon.webserver.http.RoutingRequest;

import java.util.List;
import java.util.Map;

public class RouteMatcher {
    private final Map<String, List<Method>> routers;

    public RouteMatcher(Map<String, List<Method>> routers) {
        this.routers = routers;
    }

    public boolean matches(RoutingRequest routingRequest) {
        HttpPrologue prologue = routingRequest.prologue();
        String requestPath = routingRequest.path().path();
        var requestMethod = prologue.method();
        for (Map.Entry<String, List<Method>> entry : routers.entrySet()) {
            String path = entry.getKey();
            List<Method> methods = entry.getValue();
            if (!methods.contains(requestMethod)) {
                continue;
            }
            if (path.endsWith("/**")) {
                String matchPath = path.substring(0, path.length() - 3);
                if (requestPath.startsWith(matchPath)) {
                    return true;
                }
            } else {
                if (requestPath.equals(path)) {
                    return true;
                }
            }
        }
        return false;
    }
}
