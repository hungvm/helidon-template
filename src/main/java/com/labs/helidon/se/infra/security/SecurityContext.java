package com.labs.helidon.se.infra.security;

import com.labs.helidon.se.domain.ApplicationException;
import com.labs.helidon.se.domain.ErrorCode;

public class SecurityContext {
    private static ThreadLocal<UserDetail> LOGGED_USER = new ThreadLocal<>();

    public static void setLoggedUser(UserDetail user) {
        LOGGED_USER.set(user);
    }

    public static UserDetail getLoggedUser() {
        var user = LOGGED_USER.get();
        if (user == null) {
            throw new ApplicationException(ErrorCode.INVALID_TOKEN);
        }
        return user;
    }

    public static void clear() {
        LOGGED_USER.remove();
    }
}
