package com.labs.helidon.se.domain.pokemon;

import lombok.Data;

import java.time.OffsetDateTime;

/**
 * POJO representing Pokémon.
 */
@Data
public class Pokemon {
    private int id;
    private String name;
    private int age;
    private OffsetDateTime createdAt;
}
