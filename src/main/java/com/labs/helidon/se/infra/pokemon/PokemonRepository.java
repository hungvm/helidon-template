package com.labs.helidon.se.infra.pokemon;

import com.labs.helidon.se.domain.pokemon.Pokemon;
import org.mybatis.dynamic.sql.SqlBuilder;
import org.mybatis.dynamic.sql.insert.render.InsertStatementProvider;
import org.mybatis.dynamic.sql.render.RenderingStrategies;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.labs.helidon.se.infra.pokemon.PokemonModel.pokemonModel;
import static org.mybatis.dynamic.sql.SqlBuilder.insert;
import static org.mybatis.dynamic.sql.SqlBuilder.isEqualTo;

public class PokemonRepository {
    private final NamedParameterJdbcTemplate template;

    public PokemonRepository(NamedParameterJdbcTemplate template) {
        this.template = template;
    }

    public Pokemon create(Pokemon pokemon) {
        InsertStatementProvider<Pokemon> insertStatement = insert(pokemon)
                .into(pokemonModel)
                .map(pokemonModel.name).toProperty("name")
                .map(pokemonModel.age).toProperty("age")
                .map(pokemonModel.createdAt).toProperty("createdAt")
                .build().render(RenderingStrategies.SPRING_NAMED_PARAMETER);

        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(insertStatement);
        KeyHolder keyHolder = new GeneratedKeyHolder();

        template.update(insertStatement.getInsertStatement(), parameterSource, keyHolder);
        Integer generatedKey = (Integer) keyHolder.getKeys().get("id");
        pokemon.setId(generatedKey);
        return pokemon;
    }

    public Pokemon update(Pokemon pokemon) {
        var sql = SqlBuilder.update(pokemonModel)
                .set(pokemonModel.name).equalTo(pokemon.getName())
                .set(pokemonModel.age).equalTo(pokemon.getAge())
                .where(pokemonModel.id, isEqualTo(pokemon.getId()))
                .build()
                .render(RenderingStrategies.SPRING_NAMED_PARAMETER);
        template.update(sql.getUpdateStatement(), sql.getParameters());
        return pokemon;
    }

    public void delete(int id) {
        String sql = "DELETE FROM pokemons WHERE id = :id";
        template.update(sql, Map.of("id", id));
    }

    public Optional<Pokemon> findById(int id) {
        return template.query("SELECT * from pokemons where id = :id", Map.of("id", id), (rs, rowNum) -> toEntity(rs)).stream().findFirst();
    }

    public List<Pokemon> findAll() {
        return template.query("SELECT * from pokemons", (rs, rowNum) -> toEntity(rs));
    }

    private Pokemon toEntity(ResultSet row) {
        try {
            Pokemon pokemon = new Pokemon();
            pokemon.setId(row.getInt("id"));
            pokemon.setName(row.getString("name"));
            pokemon.setAge(row.getInt("age"));
            pokemon.setCreatedAt(row.getObject("created_at", OffsetDateTime.class));
            return pokemon;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
