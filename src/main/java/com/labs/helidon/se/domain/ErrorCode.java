package com.labs.helidon.se.domain;


import lombok.Data;

@Data
public class ErrorCode {
    public static final int SC_FORBIDDEN = 403;
    public static final int SC_UNAUTHORIZED = 401;
    public static final int SC_BAD_REQUEST = 400;
    public static final int SC_NOT_FOUND = 404;
    public static final int SC_INTERNAL_SERVER_ERROR = 500;
    public static final ErrorCode FORBIDDEN = new ErrorCode(SC_FORBIDDEN, "FORBIDDEN", "This action is not allowed");
    public static final ErrorCode INVALID_TOKEN = new ErrorCode(SC_UNAUTHORIZED, "INVALID_TOKEN", "token invalid");
    public static final ErrorCode RESOURCE_NOT_FOUND = new ErrorCode(SC_NOT_FOUND, "RESOURCE_NOT_FOUND", "Resource not found");
    public static final ErrorCode INVALID_INPUT = new ErrorCode(SC_BAD_REQUEST, "INVALID_INPUT", "Input is invalid");
    public static final ErrorCode INTERNAL_SERVER_ERROR = new ErrorCode(SC_INTERNAL_SERVER_ERROR, "INTERNAL_SERVER_ERROR", "Error in backend service");
    public static final ErrorCode TRADE_QUANTITY_NOT_ENOUGH = new ErrorCode(SC_BAD_REQUEST, "TRADE_QUANTITY_NOT_ENOUGH", "Trade quantity not enough");
    public static final ErrorCode PPSE_NOT_ENOUGH = new ErrorCode(SC_BAD_REQUEST, "PPSE_NOT_ENOUGH", "ppse not enough");
    public static final ErrorCode MAX_LOAN_LIMIT = new ErrorCode(SC_BAD_REQUEST, "MAX_LOAN_LIMIT", "ppse not enough");


    private int status;
    private String code;
    private String message;

    ErrorCode(int status, String code, String message) {
        this.status = status;
        this.code = code;
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public ErrorCode setStatus(int status) {
        this.status = status;
        return this;
    }

    public String getCode() {
        return code;
    }

    public ErrorCode setCode(String code) {
        this.code = code;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public ErrorCode setCustomMessage(String message) {
        return new ErrorCode(this.status, this.code, message);
    }

    public ErrorCode setCustomMessage(String message, Object ... args) {
        this.message = String.format(message, args);
        return new ErrorCode(this.status, this.code, this.message);
    }

    public ErrorCode appendCustomMessage(String message) {
        return new ErrorCode(this.status, this.code, this.message + " " + message);
    }

    public ErrorCode format(Object ... args) {
        this.message = String.format(message, args);
        return this;
    }
}
