package com.labs.helidon.se.infra.security;

import com.labs.helidon.se.domain.ApplicationException;
import com.labs.helidon.se.domain.ErrorCode;
import io.helidon.http.HeaderNames;
import io.helidon.http.Status;
import io.helidon.webserver.http.Filter;
import io.helidon.webserver.http.FilterChain;
import io.helidon.webserver.http.RoutingRequest;
import io.helidon.webserver.http.RoutingResponse;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;

import java.security.PublicKey;
import java.util.List;

import static com.labs.helidon.se.domain.ErrorCode.INTERNAL_SERVER_ERROR;

public class JwtFilter implements Filter {
    private final RouteMatcher skipRouteMatcher;
    private final JwtParser jwtParser;

    public JwtFilter(RouteMatcher skipRouteMatcher, PublicKey publicKey) {
        this.skipRouteMatcher = skipRouteMatcher;
        jwtParser = Jwts.parser().setSigningKey(publicKey);
    }

    @Override
    public void filter(FilterChain filterChain, RoutingRequest routingRequest, RoutingResponse routingResponse) {
        if (skipRouteMatcher.matches(routingRequest)) {
            filterChain.proceed();
        } else {
            try {
                String token = getToken(routingRequest);
                var claims = jwtParser.parseClaimsJws(token).getBody();
                List<String> roles = claims.get("roles", List.class);
                UserDetail userDetail = UserDetail.of(claims, roles);
                SecurityContext.setLoggedUser(userDetail);
                filterChain.proceed();
            }
            catch (ApplicationException ex) {
                routingResponse.status(ex.getErrorCode().getStatus());
                routingResponse.send(ex.getErrorCode());
            } catch (Throwable e) {
                routingResponse.status(Status.INTERNAL_SERVER_ERROR_500);
                routingResponse.send(INTERNAL_SERVER_ERROR.setCustomMessage(e.getMessage()));
            }
            finally {
                SecurityContext.clear();
            }
        }
    }

    private String getToken(RoutingRequest routingRequest) {
        var authHeader = routingRequest.headers().first(HeaderNames.AUTHORIZATION);
        if (authHeader.isEmpty()) {
            throw new ApplicationException(ErrorCode.INVALID_TOKEN);
        }
        String token = authHeader.get();
        if (token.startsWith("Bearer ") && token.length() > 10) {
            return token.substring(7);
        }
        throw new ApplicationException(ErrorCode.INVALID_TOKEN);
    }
}
