package com.labs.helidon.se.infra.pokemon;

import com.labs.helidon.se.domain.pokemon.Pokemon;
import io.helidon.http.BadRequestException;
import io.helidon.http.NotFoundException;
import io.helidon.http.Status;
import io.helidon.webserver.http.*;
import lombok.extern.slf4j.Slf4j;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Map;

@Slf4j
public class PokemonController implements HttpService {
    private final PokemonRepository repository;

    public PokemonController(PokemonRepository repository) {
        this.repository = repository;
    }

    @Override
    public void routing(HttpRules rules) {
        rules
                .get("/pokemon", this::listPokemons)
                // Get Pokémon by ID
                .get("/pokemon/{id}", this::getPokemonById)
                // Create new Pokémon
                .post("/pokemon", Handler.create(Pokemon.class, this::insertPokemon))
                // Update name of existing Pokémon
                .put("/pokemon", Handler.create(Pokemon.class, this::updatePokemon))
                // Delete Pokémon by ID including type relation
                .delete("/pokemon/{id}", this::deletePokemonById);
    }

    private void listPokemons(ServerRequest request, ServerResponse response) {
        List<Pokemon> pokemonList = repository.findAll();
        response.send(Map.of("data", pokemonList));
    }

    private void getPokemonById(ServerRequest request, ServerResponse response) {
        int pokemonId = Integer.parseInt(request.path()
                .pathParameters()
                .get("id"));
        response.send(repository.findById(pokemonId)
                .orElseThrow(() -> new NotFoundException("Pokemon " + pokemonId + " not found")));
    }

    private void insertPokemon(Pokemon pokemon, ServerResponse response) {
        repository.create(pokemon);
        response.status(Status.CREATED_201)
                .send(pokemon);
    }

    private void updatePokemon(Pokemon pokemon, ServerResponse response) {
        repository.update(pokemon);
        response.send(pokemon);
    }

    private void deletePokemonById(ServerRequest request, ServerResponse response) {
        int id = request.path()
                .pathParameters()
                .first("id").map(Integer::parseInt)
                .orElseThrow(() -> new BadRequestException("No pokemon id"));
        var opt = repository.findById(id);
        if (opt.isEmpty()) {
            throw new NotFoundException("Pokemon " + id + " not found");
        }
        repository.delete(id);
        response.send(response);
    }
}
