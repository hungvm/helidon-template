
package com.labs.helidon.se;


import io.helidon.config.Config;
import io.helidon.http.Method;
import io.helidon.logging.common.LogConfig;
import io.helidon.webserver.WebServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The application main class.
 */
public class Main {
    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    private Main() {
    }

    public static void main(String[] args) {

        // load logging configuration
        LogConfig.configureRuntime();
        var builder = WebServer.builder();
        Server.setupMediaContext(builder);
        Map<String, List<Method>> authWhitelist = new HashMap<>();
        authWhitelist.put("/", Collections.singletonList(Method.GET));
        authWhitelist.put("/health", Collections.singletonList(Method.GET));
        authWhitelist.put("/internal/**", Collections.singletonList(Method.GET));
        authWhitelist.put("/favicon.ico", Collections.singletonList(Method.GET));
        var server = new Server(Config.global(), authWhitelist);
        builder.config(Config.global().get("server"));
        var httpServer = builder
                .routing(server::routing)
                .build()
                .start();

        Thread shutdownHook = new Thread(server::shutdown);
        Runtime.getRuntime().addShutdownHook(shutdownHook);
        LOGGER.info("Server is up! port:" + httpServer.port());

    }
}