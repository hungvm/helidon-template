package com.labs.helidon.se.infra.pokemon;

import com.labs.helidon.se.Server;
import com.labs.helidon.se.domain.pokemon.Pokemon;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class PokemonRepositoryTest extends AbstractContainerBaseTest {
    private PokemonRepository repository;
    private NamedParameterJdbcTemplate jdbcTemplate;

    @BeforeEach
    void setUp() {
        Server.migrate(appConfig);
        jdbcTemplate = Server.createDbClient(appConfig.get("db"));
        repository = new PokemonRepository(jdbcTemplate);
    }

    @AfterEach
    void tearDown() {
        jdbcTemplate.update("TRUNCATE TABLE pokemons", Map.of());
    }


    @Test
    void testCreateAndGet() {
        Pokemon pokemon = new Pokemon();
        pokemon.setName("test");
        pokemon.setAge(1);
        pokemon.setCreatedAt(OffsetDateTime.now().truncatedTo(ChronoUnit.SECONDS).withOffsetSameInstant(ZoneOffset.UTC));
        repository.create(pokemon);

        Pokemon getBack = repository.findById(pokemon.getId()).orElse(null);
        assertNotNull(getBack);
        assertEquals(pokemon, getBack);

        pokemon.setName("test 2");
        pokemon.setAge(2);
        repository.update(pokemon);

        List<Pokemon> pokemons = repository.findAll();
        assertEquals(1, pokemons.size());
        assertEquals(pokemon, pokemons.get(0));

        repository.delete(pokemon.getId());
        pokemons = repository.findAll();
        assertEquals(0, pokemons.size());
    }

    @Test
    @Disabled
    void testPerf() {
        long ts;
        int count = 100000;
        ts = System.currentTimeMillis();
        System.out.println(ts);
        for (int i = 0; i < count; i++) {
            Pokemon pokemon = new Pokemon();
            pokemon.setName("test-" + i);
            pokemon.setAge(1);
            pokemon.setCreatedAt(OffsetDateTime.now());
            repository.create(pokemon);
        }
        System.out.println(System.currentTimeMillis());
        System.out.println("create: " + (System.currentTimeMillis() - ts));
        System.out.println(System.currentTimeMillis());
        ts = System.currentTimeMillis();
        for (int i = 0; i < count; i++) {
            int t = i % (count / 10) + 1;
            var opt = repository.findById(t + 1000000);
            if (opt.isEmpty()) {
                throw new RuntimeException("not found " + t);
            }
        }
        System.out.println(System.currentTimeMillis());
        System.out.println(System.currentTimeMillis());
        System.out.println("get: " + (System.currentTimeMillis() - ts));
    }
}