package com.labs.helidon.se.infra;

import io.helidon.http.HttpPrologue;
import io.helidon.logging.common.HelidonMdc;
import io.helidon.webserver.http.Filter;
import io.helidon.webserver.http.FilterChain;
import io.helidon.webserver.http.RoutingRequest;
import io.helidon.webserver.http.RoutingResponse;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RequestLoggingFilter implements Filter {
    private static final Logger LOGGER = LoggerFactory.getLogger(RequestLoggingFilter.class);
    private static final String CORRELATION_ID_HEADER_NAME = "X-Correlation-Id";
    private static final String CORRELATION_ID_LOG_VAR_NAME = "correlationId";

    @Override
    public void filter(FilterChain filterChain, RoutingRequest routingRequest, RoutingResponse routingResponse) {
        HttpPrologue prologue = routingRequest.prologue();
        String uuid = getCorrelationIdFromHeader(routingRequest);
        HelidonMdc.set(CORRELATION_ID_LOG_VAR_NAME, uuid);
        String path = routingRequest.path().path();
        LOGGER.info("req: {} {}",prologue.method(), path);
        long startTime = System.nanoTime();
        try {
            filterChain.proceed();
        } finally {
            LOGGER.info("res: {} {} - {} - {}ms", prologue.method(), path, routingResponse.status().code(), (System.nanoTime() - startTime) / 1000000);
            HelidonMdc.remove(CORRELATION_ID_LOG_VAR_NAME);
        }
    }

    private static String getCorrelationIdFromHeader(final RoutingRequest request) {
        String correlationId = "";
        for (var x : request.headers()) {
            if (x.name().equalsIgnoreCase(CORRELATION_ID_HEADER_NAME)) {
                correlationId = x.get();
            }
        }
        if (StringUtils.isBlank(correlationId)) {
            correlationId = generateUniqueCorrelationId();
        }
        return correlationId;
    }

    private static String generateUniqueCorrelationId() {
        return RandomStringUtils.randomAlphanumeric(10);
    }
}
