package com.labs.helidon.se.infra.health;

import com.labs.helidon.se.Main;
import io.helidon.webserver.http.HttpRules;
import io.helidon.webserver.http.HttpService;
import io.helidon.webserver.http.ServerRequest;
import io.helidon.webserver.http.ServerResponse;

import java.util.Map;
import java.util.Optional;

public class HealthController implements HttpService {
    @Override
    public void routing(HttpRules httpRules) {
        httpRules
                .get("/", this::get)
                .get("/health", this::getHealth);
    }

    private void get(ServerRequest request, ServerResponse response) {
        Optional<String> version = Optional.ofNullable(
                Main.class.getPackage().getImplementationVersion());
        response.send(Map.of("version", version.orElse("dev")));
    }

    private void getHealth(ServerRequest request, ServerResponse response) {
        Optional<String> version = Optional.ofNullable(
                Main.class.getPackage().getImplementationVersion());
        response.send(Map.of("version", version.orElse("dev")));
    }
}
