package com.labs.helidon.se.infra.pokemon;

import com.labs.helidon.se.Server;
import com.labs.helidon.se.domain.pokemon.Pokemon;
import io.helidon.http.Status;
import io.helidon.webclient.api.ClientResponseTyped;
import io.helidon.webclient.http1.Http1Client;
import io.helidon.webserver.WebServerConfig;
import io.helidon.webserver.http.HttpRouting;
import io.helidon.webserver.testing.junit5.DirectClient;
import io.helidon.webserver.testing.junit5.RoutingTest;
import io.helidon.webserver.testing.junit5.SetUpRoute;
import io.helidon.webserver.testing.junit5.SetUpServer;
import org.junit.jupiter.api.Test;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RoutingTest
public class PokemonControllerTest {

    private final Http1Client client;

    protected PokemonControllerTest(DirectClient client) {
        this.client = client;
    }

    private static PokemonRepository pokemonRepository;

    @SetUpRoute
    static void routing(HttpRouting.Builder builder) {
        pokemonRepository = mock(PokemonRepository.class);
        PokemonController controller = new PokemonController(pokemonRepository);
        builder.register("/", controller);
    }

    @SetUpServer
    static void server(WebServerConfig.Builder builder) {
        Server.setupMediaContext(builder);
    }

    @Test
    void test() {
        Pokemon pokemon = new Pokemon();
        pokemon.setId(1);
        pokemon.setName("test");
        pokemon.setAge(1);
        pokemon.setCreatedAt(OffsetDateTime.now().truncatedTo(ChronoUnit.MILLIS).withOffsetSameInstant(ZoneOffset.UTC));
        when(pokemonRepository.findAll()).thenReturn(List.of(pokemon));
        ClientResponseTyped<PokemonControllerTestIT.PokemonResponse> response = client.get("/pokemon").request(PokemonControllerTestIT.PokemonResponse.class);
        assertEquals(response.status(), Status.OK_200);
        List<Pokemon> pokemons = response.entity().getData().stream().toList();
        assertEquals(1, pokemons.size());
        assertEquals(pokemon, pokemons.getFirst());
    }
}
