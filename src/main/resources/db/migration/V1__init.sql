CREATE SEQUENCE public.id_seq
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1000000
    CACHE 1;

CREATE TABLE pokemons
(
    id         int DEFAULT nextval('id_seq') NOT NULL PRIMARY KEY,
    name       VARCHAR(64)                   NOT NULL,
    age        int,
    created_at timestamp with time zone
);
