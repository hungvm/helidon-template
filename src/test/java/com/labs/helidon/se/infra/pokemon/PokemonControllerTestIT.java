package com.labs.helidon.se.infra.pokemon;

import com.labs.helidon.se.Server;
import com.labs.helidon.se.domain.pokemon.Pokemon;
import io.helidon.common.context.Contexts;
import io.helidon.http.Method;
import io.helidon.http.Status;
import io.helidon.webclient.api.ClientResponseTyped;
import io.helidon.webclient.http1.Http1Client;
import io.helidon.webserver.WebServerConfig;
import io.helidon.webserver.http.HttpRouting;
import io.helidon.webserver.testing.junit5.ServerTest;
import io.helidon.webserver.testing.junit5.SetUpRoute;
import io.helidon.webserver.testing.junit5.SetUpServer;
import lombok.Data;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@ServerTest
class PokemonControllerTestIT extends AbstractContainerBaseTest {

    private final Http1Client client;
    private static Server server;

    protected PokemonControllerTestIT(Http1Client client) {
        this.client = client;
    }

    @SetUpRoute
    static void routing(HttpRouting.Builder builder) {
        server.routing(builder);
    }

    @SetUpServer
    static void server(WebServerConfig.Builder builder) {
        Map<String, List<Method>> map = new HashMap<>();
        map.put("/**", List.of(Method.GET, Method.POST, Method.PUT, Method.PATCH, Method.DELETE));
        server = new Server(appConfig, map);
        Server.setupMediaContext(builder);
    }

    @BeforeEach
    void setup() {
        PokemonRepository pokemonRepository = Contexts.globalContext().get(PokemonRepository.class).orElseThrow(() -> new RuntimeException("PokemonRepository not found"));
        Pokemon pokemon = new Pokemon();
        pokemon.setName("Bulbasaur");
        pokemon.setAge(1);
        pokemon.setCreatedAt(OffsetDateTime.now());
        pokemonRepository.create(pokemon);
        pokemon = new Pokemon();
        pokemon.setName("Charmander");
        pokemon.setAge(1);
        pokemon.setCreatedAt(OffsetDateTime.now());
        pokemonRepository.create(pokemon);
        pokemon = new Pokemon();
        pokemon.setName("Squirtle");
        pokemon.setAge(1);
        pokemon.setCreatedAt(OffsetDateTime.now());
        pokemonRepository.create(pokemon);
        pokemon = new Pokemon();
        pokemon.setName("Caterpie");
        pokemon.setAge(1);
        pokemon.setCreatedAt(OffsetDateTime.now());
        pokemonRepository.create(pokemon);
    }

    @AfterEach
    void tearDown() {
        PokemonRepository pokemonRepository = Contexts.globalContext().get(PokemonRepository.class).orElseThrow(() -> new RuntimeException("PokemonRepository not found"));
        for (Pokemon pokemon : pokemonRepository.findAll()) {
            pokemonRepository.delete(pokemon.getId());
        }
    }

    @Test
    void testListAllPokemons() {
        ClientResponseTyped<PokemonResponse> response = client.get("/pokemon").request(PokemonResponse.class);
        assertThat(response.status(), is(Status.OK_200));
        List<String> names = response.entity().getData().stream().map(Pokemon::getName).toList();
        assertThat(names, is(List.of("Bulbasaur", "Charmander", "Squirtle", "Caterpie")));
    }

    @Data
    public static class PokemonResponse {
        private List<Pokemon> data;
    }
}