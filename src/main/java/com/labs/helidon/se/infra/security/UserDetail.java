package com.labs.helidon.se.infra.security;

import io.jsonwebtoken.Claims;
import lombok.Data;

import java.util.List;

@Data
public class UserDetail {
    String subject;
    String customerEmail;
    String customerMobile;
    String custodyCode;
    String customerId;
    String investorId;
    String userId;
    String brokerId;
    private List<String> roles;

    public static UserDetail of(Claims claims, List<String> authorities) {
        var principal = new UserDetail();
        principal.setCustomerEmail(claims.get("customerEmail", String.class));
        principal.setCustomerMobile(claims.get("customerMobile", String.class));
        principal.setCustodyCode(claims.get("custodyCode", String.class));
        principal.setCustomerId(claims.get("customerId", String.class));
        principal.setRoles(authorities);
        principal.setInvestorId(claims.get("investorId", String.class));
        principal.setUserId(claims.get("userId", String.class));
        principal.setBrokerId(claims.get("brokerId", String.class));
        principal.setSubject(claims.getSubject());
        return principal;
    }
}
