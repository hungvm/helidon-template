package com.labs.helidon.se.infra.pokemon;

import org.mybatis.dynamic.sql.SqlColumn;
import org.mybatis.dynamic.sql.SqlTable;

import java.sql.JDBCType;
import java.time.OffsetDateTime;

public class PokemonModel extends SqlTable {
    public static final PokemonModel pokemonModel = new PokemonModel();
    public final SqlColumn<Integer> id = column("id");
    public final SqlColumn<String> name = column("name");
    public final SqlColumn<Integer> age = column("age");
    public final SqlColumn<OffsetDateTime> createdAt = column("created_at");

    public PokemonModel() {
        super("pokemons");
    }
}
